import boto3
import os
from boto3.dynamodb.conditions import Key, Attr

def lambda_handler(event, context):
    """Function to retrieve the posts
		Parameters: 2, an event and context
    return: the value of the items key
		"""

    postId = event["postId"]    
    dynamodb = boto3.resource('dynamodb') #save the dynamodb resource to a variable
    table = dynamodb.Table(os.environ['DB_TABLE_NAME']) #save the environment variable of the dynamodb table on a variable for simplicity
    
    if postId=="*": #all
        items = table.scan()
    else:
        items = table.query(
            KeyConditionExpression=Key('id').eq(postId)
        )
    
    return items["Items"]
