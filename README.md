# Polly

Polly is an AWS services that convert text to a voice speech. It try to keep a robotic voice out and instead go for a more human speech as possible.

**What is this?**

This is an example (a proof of concept) to test how Polly works. Using a simple web inteface to push and scan words written in a text input. 
It use DynamoDB to store the resources.

**How to use it**?

You would need to create your own AWS resources in order to this project to work:
- An Dynamodb Database with respective "tables".
- An S3 bucket
- No server or instance needed but a Lambda function with respective trigger.

